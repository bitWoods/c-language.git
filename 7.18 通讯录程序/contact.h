#pragma once
#include <stdio.h>
#include <memory.h>
#include <string.h>
#define MAX_NAME 20
#define MAX_SEX 20
#define MAX_TEL 20
#define MAX 20

#define MAX_ADDRE 20
//姓名、性别、年龄、电话、住址
typedef struct peoinfo     //个人信息表
{
	char NAME[MAX_NAME];
	char SEX[MAX_SEX];
	int AGE;
	char tel[MAX_TEL];
	char ADDRE[MAX_ADDRE];
}peoinfo;

typedef struct contact   //通讯录
{
	peoinfo data[MAX];  //存放添加进来的人的信息
	int sz; //记录有效人数
}contact;


//初始化通讯录  
void initcontact(contact* pc);

//添加通讯录
void addcontact(contact * pc);
//打印通讯录
void printcontact( const contact * pc);

//删除通讯录
void delcontact(contact* pc);

//查找通讯录
void searchcontact(contact * pc);

//修改通讯录
void modifycontact(contact * pc);

//清空所以联系人
void alldelcontact(contact * pc);
