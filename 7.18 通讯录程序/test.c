#define _CRT_SECURE_NO_WARNINGS 1
#include "contact.h"

void menu()
{
	printf("**1.add   2.del*******\n");
	printf("**3.search    4. modify*******\n");
	printf("**5.sort      6.print*****\n");
	printf("**7.alldel     \n");

	printf("*****0.exit*****\n");

}

enum option
{
	exit,
	add,
	del,
	search,
	modify,
	sort,
	print,
	alldel
};

int main()
{
	int input = 0;
	contact con;
	//初始化通讯录
	initcontact(&con);
	do
	{
		menu();
		printf("请选择：>");
		scanf("%d",&input);
		switch (input)
		{
		case add:
			addcontact(&con);
			break;
		case del:
			delcontact(&con);
			break;
		case search:
			searchcontact(&con);
			break;
		case modify:
			modifycontact(&con);
			break;
		case sort:

			break;
		case print:
			printcontact(&con);
			break;
		case alldel:
			alldelcontact(&con);
			break;
		case exit:
			break;
		default:
			break;
		}
	} while (input);
	return 0;
}