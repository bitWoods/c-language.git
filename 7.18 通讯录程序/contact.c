#define _CRT_SECURE_NO_WARNINGS 1
#include "contact.h"


//初始化通讯录   
void initcontact(contact* pc) 
{
	pc->sz = 0;
	//memset();  内存设置
	memset(pc->data,0,sizeof(pc->data));
}


//增加联系人
void addcontact(contact* pc) 
{
	if (pc->sz == MAX)
	{
		printf("通讯录已经满了\n");
		return;
	}
	//增加一个信息

	printf("请输入名字:>\n");
	scanf("%s",pc->data[pc->sz].NAME);
	printf("请输入年龄:>\n");
	scanf("%d", &(pc->data[pc->sz].AGE));
	printf("请输入性别:>\n");
	scanf("%s", pc->data[pc->sz].SEX);
	printf("请输入电话:>\n");
	scanf("%s", pc->data[pc->sz].tel);
	printf("请输入住址:>\n");
	scanf("%s", pc->data[pc->sz].ADDRE);
	pc->sz++;
	printf("增加成功\n");
}


void printcontact(const contact* pc)
{
	int  i = 0; 
	//打印标题
	printf("%-10s\t%-5s\t%-10s\t%-10s\t%-10s\n","名字", "年龄","性别","电话" ,"地址");

	//打印数据
	for ( i = 0; i < pc->sz; i++)
	{
		printf("%-10s\t%-5d\t%-10s\t%-10s\t%-10s\n",
			pc->data[i].NAME, 
			pc->data[i].AGE, 
			pc->data[i].SEX, 
			pc->data[i].tel,
			pc->data[i].ADDRE);
	}
}

static int findepeo(contact *pc , char name[])
{
	int i = 0;
	for ( i = 0; i <pc->sz; i++)
	{
		if (strcmp(pc->data[i].NAME,name)==0)
		{
			return i;
		}

		
	}
	return -1;

	
}

//del
void delcontact(contact* pc)
{
	char name[MAX_NAME] = { 0 };

	if (pc->sz == 0)
	{
		printf("通讯录已经没人了\n");
	}
	printf("请输入要删除人的名字：>");
	scanf("%s",name);
	//1.查找要删除的人
	int pos = findepeo(pc,name);
	if (pos == -1) {
		printf("要删除的人不存在\n");
		return;
	}
	//2.删除
	int i = 0;
	for ( i = pos; i < pc->sz-1; i++)
	{
		pc->data[i] = pc->data[i + 1];
	}
	pc->sz--;
	printf("删除成功");
}

void searchcontact(contact* pc)
{
	char name[MAX_NAME] = { 0 };
	printf("请输入要查找人的名字：>");
	scanf("%s", name);
	//1.查找要查找的人
	int pos = findepeo(pc, name);
	if (pos == -1) {
		printf("要查找的人不存在\n");
		return;
	}
	else
	{
		printf("%-10s\t%-5s\t%-10s\t%-10s\t%-10s\n", "名字", "年龄", "性别", "电话", "地址");

		//打印数据

			printf("%-10s\t%-5d\t%-10s\t%-10s\t%-10s\n",
				pc->data[pos].NAME,
				pc->data[pos].AGE,
				pc->data[pos].SEX,
				pc->data[pos].tel,
				pc->data[pos].ADDRE);
		
	}
}

void modifycontact(contact* pc)
{
	char name[MAX_NAME] = { 0 };
	printf("请输入要修改人的名字：>");
	scanf("%s", name);
	//1.查找要查找的人
	int pos = findepeo(pc, name);
	if (pos == -1) {
		printf("要修改的人不存在\n");
		return;
	}
	else
	{
		printf("%-10s\t%-5s\t%-10s\t%-10s\t%-10s\n", "名字", "年龄", "性别", "电话", "地址");

		//打印数据
		printf("请输入名字:>\n");
		scanf("%s", pc->data[pos].NAME);
		printf("请输入年龄:>\n");
		scanf("%d", &(pc->data[pos].AGE));
		printf("请输入性别:>\n");
		scanf("%s", pc->data[pos].SEX);
		printf("请输入电话:>\n");
		scanf("%s", pc->data[pos].tel);
		printf("请输入住址:>\n");
		scanf("%s", pc->data[pos].ADDRE);
		printf("修改成功\n");
		
	}
}

//清空所有联系人
void alldelcontact(contact* pc)
{
	if (pc->sz == 0)
	{
		printf("通讯录已经没人了,不需要清空\n");
	}
	int i = 0;
	for ( i = 0; i < pc->sz; i++)
	{
		pc->data[pc->sz]= '\0';
	}
	printf("已经全部清空");
}