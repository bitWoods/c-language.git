#define _CRT_SECURE_NO_WARNINGS 1
#include "game.h"
void InitBoard(char board[ROWS][COLS], int rows, int cols,char set) 
{
	int i = 0;
	int j = 0;
	for ( i = 0; i < rows; i++)
	{
		for ( j = 0; j < cols; j++)
		{
			board[i][j] = set;
		}
	}
}
void DisPlayBoard(char board[ROWS][COLS], int row, int col) 
{
	int i = 0;
	int j = 0;
	printf("------------扫雷游戏-------------\n");
	//打印列号
	for ( i = 0; i <= col; i++)
	{
		printf("%d ",i);
	}
	printf("\n");

	for ( i = 1; i <= row; i++)
	{
		printf("%d ",i);
		for ( j = 1; j <= col; j++)
		{
			printf("%c ",board[i][j]);
		}
		printf("\n");
	}
	printf("------------扫雷游戏-------------\n");

}
//布置雷
void SetMine(char mine[ROWS][COLS], int row, int col)
{
	//布置10个雷
	int count = EASY_Count;
	while (count)
	{
		//生产随机的下标                                                                           
		int x = rand() % row + 1;
		int y = rand() % col + 1;
		if (mine[x][y] == '0') { //看看当前格子是不是空的

			mine[x][y] = '1';	//如果是空的就种一颗雷

			count--;
		}
	}
}
//雷的个数信息
static int get_mine_count(char mine[ROWS][COLS],int x  , int y )
{
	return mine[x - 1][y] +
		mine[x - 1][y - 1] +
		mine[x][y - 1] +
		mine[x + 1][y - 1] +
		mine[x + 1][y] +
		mine[x + 1][y + 1] +
		mine[x][y + 1] +
		mine[x - 1][y + 1] - 8 * '0';
}

void FindMine(char mine[ROWS][COLS], char show[ROWS][COLS], int row, int col)
{
	//1、输入排查的坐标
	//2.检查坐标处是不是雷
		//(1) 是雷 = 炸死了
		//(2) 不是雷 - 统计坐标周围有几个雷- 存储排查雷的信息到show数组,游戏继续
	int x = 0;
	int y = 0;
	int win = 0; //定义一个变量，如果全部排查完了就 提示排查成功
	while (win < row*col - EASY_Count)  //game.h 定义的
	{
		printf("请输入要排查雷的坐标");
		scanf("%d%d", &x, &y);
		
		if (x >= 1 && x <= row && y >= 1 && y <= col)
		{
			if (mine[x][y] == '1')
			{
				printf("很遗憾你被炸死了\n");
				DisPlayBoard(mine, row, col);
				break;
			}
			else
			{
				//不是雷 统计周围有几个雷,xy
				int count = get_mine_count(mine,x,y);
				show[x][y]=count+'0';
				//显示排查的信息
				DisPlayBoard(show,row,col);
				win++;  //每进来一次就 记录一次
			}
		}
		else
		{
			printf("坐标不合法,请重新输入\n");
		}
	}	
	if (win==row * col -EASY_Count)  //如果计数的等于 空格子数就赢了
	{
		printf("恭喜你排雷成功\n");
		DisPlayBoard(mine, row, col);//看一下怎么炸死的
	}
}