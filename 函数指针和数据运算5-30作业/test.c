/*#d*//*efine _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>
#include <stdlib.h>*/
//int main()
//{
//	int aa[2][5] = { 10,9,8,7,6,5,4,3,2,1 };
//	int* ptr1 = (int*)(&aa + 1);
//	int* ptr2 = (int*)(*(aa + 1));
//	printf("%d,%d", *(ptr1 - 1), *(ptr2 - 1));
//	return 0;
//}


//struct  s    //定义结构体
//{
//	char name[20];
//	int age;
//};
//
//int asc_cmp(const void * e1, const void* e2)   //数组排序-升序
//{
//	return *(int*)e1 - *(int*)e2;
//}
//
//int str_cmp_age(const void* e1, const void* e2)    //结构体按照年龄排序
//{
//
//	return ((struct s*)e1)->age - ((struct s*)e2) ->age ;
//} 
//
//int str_cmp_name(const void* e1,const void *e2)     //结构体按照名字排序
//{
//	return strcmp(((struct s*)e1)->name , ((struct s*)e2)->name);
//}
//
//void test()                     //数组排序-升序
//{
//	int  i = 0;
//	int arr[10] = { 9,8,7,6,5,4,3,2,1,0 };
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	qsort(arr,sz,4,asc_cmp);
//	for ( i = 0; i < sz; i++)
//	{
//		printf("%d ",arr[i]);
//	}
//}
//
//
//void test2()  //结构体按照字符排序
//{
//	
//	struct  s ss[3] = { {"zhasan",22},{"lisi",20},{"wangwu",30} };
//	int sz = sizeof(ss)/sizeof(ss[0]);
//	qsort(ss,sz,sizeof(ss[0]),str_cmp_age);
//
//}
// 
//void test3()
//{
//	struct  s ss[3] = { {"zhasan",22},{"lisi",20},{"wangwu",30} };
//	int sz = sizeof(ss) / sizeof(ss[0]);
//	qsort(ss, sz, sizeof(ss[0]), str_cmp_name);
//}
//
//int main()
//{
//
//	test();//数组排序-升序
//	test2();  //结构体按照年龄排序
//	test3();   //结构体按照字符排序
//	return 0;
//}
//
//struct  s    //定义结构体
//{
//	char name[20];
//	int age;
//};

//void test2()  //结构体按照字符排序
//{
//	
//	struct  s ss[3] = { {"zhasan",22},{"lisi",20},{"wangwu",30} };
//	int sz = sizeof(ss)/sizeof(ss[0]);
//	my_qsort(ss,sz,sizeof(ss[0]),str_cmp_age);
//
//}
 
//void test3()
//{
//	struct  s ss[3] = { {"zhasan",22},{"lisi",20},{"wangwu",30} };
//	int sz = sizeof(ss) / sizeof(ss[0]);
//	my_qsort(ss, sz, sizeof(ss[0]), str_cmp_name);
//}
//
//
//int str_cmp_age(const void* e1, const void* e2)    //结构体按照年龄排序
//{
//
//	return ((struct s*)e1)->age - ((struct s*)e2) ->age ;
//} 
//
//
//
//int asc_cmp(const void * e1, const void* e2)   //数组排序-升序
//{
//	return *(int*)e1 - *(int*)e2;
//}


#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>
#include <stdlib.h>

int asc_cmp(const void* e1, const void* e2)   //数组排序-升序
{
	return *(int*)e1 - *(int*)e2;
}




void  Swap(char* buf1, char* buf2, int onesize)  //升序交换
{
	int  i = 0;
	for (i = 0; i < onesize; i++)
	{
		char* temp = *buf1;
		*buf1 = *buf2;
		*buf2 = temp;
		buf1++;
		buf2++;
	}
}
void* my_qsort(void* base, int gs, int onesize, int (*cmp)(const void* e1, const void* e2))   //参数   1.待排序的数组地址   2.数组中代排序的元素数量  3.各元素占用的空间大小 4.函数指针用于确定排序的顺序 
{
	int  i = 0;
	int  j = 0;
	for (i = 0; i < gs - 1; i++)
	{
		for (j = 0; j < gs - 1 - i; j++)
		{
			if (cmp((char*)base + j * onesize, (char*)base + (j + 1) * onesize) > 0)
			{
				//交换
				Swap((char*)base + j * onesize, (char*)base + (j + 1) * onesize, onesize);
			}
		}
	}
}


void my_qsort_test()                     //数组排序-升序
{
	int  i = 0;
	int arr[10] = { 9,8,7,6,5,4,3,2,1,0 };
	int sz = sizeof(arr) / sizeof(arr[0]);
	my_qsort(arr, sz, 4, asc_cmp);
	for (i = 0; i < sz; i++)
	{
		printf("%d ", arr[i]);
	}
}
int main()
{
	//my_qsort();
	my_qsort_test();
	return 0;
}
