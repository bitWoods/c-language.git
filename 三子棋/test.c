#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>
#include "game.h"
void meun()
{
	printf("******************\n");
	printf("****  1.play ******\n");
	printf("****  0.exit *****\n");
	printf("******************\n");

}

void game()
{
	//存储数据 - 二维数组
	char board[ROW][COL];
	//初始化棋盘 - 初始化空格
	InitBorad(board,ROW,COL);

	//打印棋盘 - 本质是打印数组内容
	DisplayBoard(board ,ROW,COL);
	char ret = 0;   //接受游戏状态 
	while (1)
	{
		//玩家玩
		PlayerMove(board, ROW, COL);
		DisplayBoard(board, ROW, COL);  //玩家下好棋 打印棋盘

		ret = IsWin(board, ROW, COL);    //判断输赢
		if (ret != 'C') //C是我们的继续，如果我们不是C 那就是其他的结局 那么游戏结束
		{
			break;
		}
		//电脑玩
		ComputerMover(board, ROW, COL);
		DisplayBoard(board, ROW, COL);  //电脑下好棋 打印棋盘
		ret = IsWin(board, ROW, COL);	//判断输赢
		if (ret != 'C') //C是我们的继续，如果我们不是C 那就是其他的结局 那么游戏结束
		{
			break;
		}
		
	}
	if (ret == '*')
	{
		printf("玩家赢了\n");

	}
	else if (ret == '#')
	{
		printf("电脑赢了\n");

	}else
	{
		printf("平局\n");
	}
	DisplayBoard(board, ROW, COL);  //电脑下好棋 打印棋盘
}
int main()
{
	int input = 0;
	srand((unsigned int)time(NULL));
	do
	{
		meun();
		printf("请输入你的选择>");
		scanf("%d", &input);
		switch (input)
		{
		case 1:
			game();
			break;
		case 0:
			printf("退出游戏\n");
			break;
		default:
			printf("选择错误请重新选择\n");
			break;
		}
	} while (input);  


	return  0;
}