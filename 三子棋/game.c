#define _CRT_SECURE_NO_WARNINGS 1
#include "game.h"

//初始化棋盘
void InitBorad(char borad[ROW][COL], int row, int col)
{
		int i = 0;
		int j = 0;
		for (i=0 ; i < row; i++)
		{
			for ( j = 0; j < col; j++)
			{
				borad[i][j] = ' ';
			}
		}
}
//打印棋盘
void DisplayBoard(char board[ROW][COL], int row, int col)
{
	 
	int i = 0;
	for (i = 0; i < row; i++)
	{
		int j = 0;
		for (j = 0; j < col; j++)
		{
			printf(" %c ", board[i][j]);//打印的那一行的数据
			if (j < col - 1) //为了最后一行不打印
				printf("|");
		}
		printf("\n");
		if (i < row - 1)
		{
			int j = 0;
			for (j = 0; j < col; j++)
			{
				printf("---");
				if (j < col - 1)
					printf("|");
			}
			printf("\n");
		}
	}
}

void PlayerMove(char board[ROW][COL], int row, int col) 
{
	int x = 0;
	int y = 0;
	printf("玩家下>\n");
	while (1)
	{
		printf("请输入下棋的坐标:>");
		scanf("%d %d", &x, &y);
		if (x>=1 && x<=row && y>=1 && y<=col)  //判断下棋落子 是否在坐标内  
		{
			if (board[x - 1][y - 1] == ' ') //因为用户 并不是程序员不知道数组是从0开始的，所以减1
			{
				board[x - 1][y - 1] = '*';  //用户下子 *代替
				break;
			}
			else
			{
				printf("坐标已有棋,请重新输入\n");
			}
		}
		else
		{
			printf("坐标非法,请重新输入\n");
		}
	}
}
void ComputerMover(char board[ROW][COL], int row, int col)	
{
	printf("电脑下:>\n");
	while (1)
	{
		int x = rand() % row;   //%3 就是余数0-2
		int y = rand() % col;
		if (board[x][y] ==' ') //电脑判断是否是可下的地方
		{
			board[x][y] = '#'; //电脑下棋
			break;
		}

	}

}

int IsFull(char board[ROW][COL], int row, int col)
{
	int i = 0;
	int j = 0;
	for ( i = 0; i < row; i++)
	{
		for ( j = 0; j < col; j++)
		{
			if (board[i][j] == ' ')
			{
				return 0;
			}
		}
	}
	return 1;
}
//判断游戏是否有输赢
char IsWin(char board[ROW][COL], int row, int col)
{
	int i = 0;
	//判断三行
	for (i = 0; i < row; i++)
	{
		if (board[i][0] == board[i][1] && board[i][1] == board[i][2] && board[i][1] !=' ')
		{
			return board[i][1]; //不管里面是什么元素 都要返回回去
		}
	}
	//判断三列
	for (i = 0; i < col; i++)
	{
		if (board[0][i] == board[1][i] && board[1][i] == board[2][i] && board[2][i] != ' ')
		{
			return board[1][i]; //不管里面是什么元素 都要返回回去
		}
	}
	//对角线
	if(board[0][0] == board[1][1]&& board[1][1]==board[2][2]&&board[1][1]!=' ')
	{
		return board[1][1];
	}
	//对角线
	if (board[0][2] == board[1][1] && board[1][1] == board[2][0] && board[1][1] != ' ')
	{
		return board[1][1];
	}
	//判断平局
	//如果棋盘满了返回1 ，不满返回 0
	int ret = IsFull(board, row, col);   //是否棋盘满

		if (ret ==  1 )
		{
			return 'Q';
		}

		//继续
		return 'C';
}