#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>


//int add(int x, int y)
//{
//	return x + y;
//}
//int sub(int x, int y)
//{
//	return x - y;
//}
//int mul(int x, int y)
//{
//	return x * y;
//}
//int div(int x, int y)
//{
//	return x / y;
//}

//void  menu()
//{
//	printf("************");
//	printf("***1.ADD***\n");
//	printf("***2.SUB***\n");
//	printf("***3.MUL***\n");
//	printf("***4.DIV***\n");
//	printf("***0.exit***\n");
//}
//int main() 
//{
//	int input = 0;
//	int ret = 0;
//	int x, y;
//	int(*pfArr[5])(int , int ) = { NULL, add,sub,mul,div};
//	do
//	{
//		menu();
//		printf("请选择菜单");
//		scanf("%d", &input);
//		if (input>=1 && input<= 4)
//		{
//			printf("请输入2个数字");
//			scanf("%d %d",&x , &y);
//			ret = (pfArr[input])( x ,  y);
//			printf("ret = %d\n", ret);
//		}
//		else
//		{
//			printf("输入错误\n");
//		}
//		
//	} while (input);
//	return 0;
//}



//#include <stdio.h>
//int add(int a, int b)
//{
//	return a + b;
//}
//int sub(int a, int b)
//{
//	return a - b;
//}
//int mul(int a, int b)
//{
//	return a * b;
//}
//int div(int a, int b)
//{
//	return a / b;
//}
//
//int main()
//{
//	int x, y;
//	int input = 1;
//	int ret = 0;
//	do
//	{
//		printf("*************************\n");
//		printf(" 1:add 2:sub \n");
//		printf(" 3:mul 4:div \n");
//		printf("*************************\n");
//		printf("请选择：");
//		scanf("%d", &input);
//		switch (input)
//		{
//		case 1:
//			printf("输入操作数：");
//			scanf("%d %d", &x, &y);
//			ret = add(x, y);
//			printf("ret = %d\n", ret);
//			break;
//		case 2:
//			printf("输入操作数：");
//			scanf("%d %d", &x, &y);
//			ret = sub(x, y);
//			printf("ret = %d\n", ret);
//			break;
//		case 3:
//			printf("输入操作数：");
//			scanf("%d %d", &x, &y);
//			ret = mul(x, y);
//			printf("ret = %d\n", ret);
//			break;
//		case 4:
//			printf("输入操作数：");
//			scanf("%d %d", &x, &y);
//			ret = div(x, y);
//			printf("ret = %d\n", ret);
//			break;
//		case 0:
//			printf("退出程序\n");
//			break;
//		default:
//			printf("选择错误\n");
//			break;
//		}
//	} while (input);
//	return 0;
//}


//void test(const char* str)
//{
//	printf("%s\n", str);
//}
//int main()
//{
//	//函数指针pfun
//	void (*pfun)(const char*) = test;
//	//函数指针的数组pfunArr
//	void (*pfunArr[5])(const char* str);
//	pfunArr[0] = test;
//	//指向函数指针数组pfunArr的指针ppfunArr
//	void (*(*ppfunArr)[10])(const char*) = &pfunArr;
//	return 0;
//}

int add(int x, int y)
{
	return x + y;
}
int sub(int x, int y)
{
	return x - y;
}
int mul(int x, int y)
{
	return x * y;
}
int div(int x, int y)
{
	return x / y;
}

int calu(int(*(*pf)(int,int))) 
{
	int x, y;
	printf("输入操作数：");
	scanf("%d %d", &x, &y);

	return (*pf)(x, y);
}

int main()
{
	
	int input = 1;
	int ret = 0;
	do
	{
		printf("*************************\n");
		printf(" 1:add 2:sub \n");
		printf(" 3:mul 4:div \n");
		printf("*************************\n");
		printf("请选择：");
		scanf("%d", &input);
		switch (input)
		{
		case 1:
			
			ret = calu(add);
			printf("ret = %d\n", ret);
			break;
		case 2:
			ret = calu(sub);
			printf("ret = %d\n", ret);
			break;
		case 3:
			ret = calu(mul);
			printf("ret = %d\n", ret);
			break;
		case 4:
			ret = calu(div);
			printf("ret = %d\n", ret);
			break;
		case 0:
			printf("退出程序\n");
			break;
		default:
			printf("选择错误\n");
			break;
		}
	} while (input);
	return 0;
}


