#define _CRT_SECURE_NO_WARNINGS 1


#include <stdio.h>
#include <stdlib.h>
int main()
{
	//开辟一个10个整形的空间   10* sizeof(int)
	int arr[10]; // 局部变量  在栈区
	int* p=	(int *)malloc(10 * sizeof(int));   //在堆区
	//使用这些空间的时候
	if (p == NULL)  //如果是空指针 那么就是没有开辟成功 
	{
		perror("main"); //报错提示
		return 0;
	}
	//使用
	int i = 0;
	for ( i = 0; i < 10; i++)
	{
		*(p + i) = i;  //0-10
	}

	//realloc 调整空间
	int *ptr = realloc(p, 20 * sizeof(int));
	int	* a= (int*)realloc(NULL, 40);
	//回收空间
	free(p);
	p = NULL;
	return 0;
}