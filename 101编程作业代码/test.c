#define _CRT_SECURE_NO_WARNINGS 

#include <stdio.h>

//----------------------------------------   打印一个数的个数
//void print(unsigned int n)
//{
//    if (n > 9)
//        print(n / 10);
//    printf("%d ", n % 10);
//}
//int main()
//{
//        unsigned n = 658;
//        print(n);
//        return 0;
//  
//}
// 
// 

//------------------------------------------   字符串逆序 普通版本
//int my_strlen(char * arr)
//{
//	int count = 0;
//	while (*arr != '\0')
//	{
//			count++;
//			arr++;
//	}
//	return count;
//	
//}
//void reverse(char *arr) 
//{
//	char left = 0;
//	char right = my_strlen(arr)-1;
//	while (left<right)
//	{
//		char temp = arr[left];
//		arr[left]= arr[right];
//		arr[right]=temp;
//		left++;
//		right--;
//	}
//
//}
//int main()
//{
//	char arr[] = "abcdef";
//	reverse(arr);
//	printf("%s",arr);
//	return 0;
//}


//reverse_string(char* str)
//{
//	char temp = *str;   //  拿出第一个 字符
//	int len = my_strlen(str);
//	*str= *(str + len - 1);  //把最后一个放在最前来
//	*(str + len - 1) = '\0';  //把那个位置放\0
//	if (my_strlen(str+1)>=2)
//	{
//		reverse_string(str + 1); 
//
//	}
//	*(str + len - 1) = temp;	//
//}
//int main()
//{
//	char arr[] = "abcdef";
//	reverse_string(arr);
//	printf("%s",arr);
//	return 0;
//}

//----------------------------  一个数一个数的相加
//int DigitSum(int n)//1729
//{
//	if (n > 9)
//		return DigitSum(n / 10) + n % 10;
//	else
//		return n;
//}
//int  main()
//{
//	int num = 1729;
//	int sum = DigitSum(num);
//	printf("%d",sum);
//	return 0;
//}


//---------------------------  编写一个函数实现n的k次方，使用递归实现。
//double Pow(int n, int k)
//{
//	if (k == 0)
//		return 1;
//	else if (k > 0)
//
//		return n * Pow(n, k - 1);
//	else
//		return 1.0 / (Pow(n, -k));
//
//}
//int main()
//{
//	int n = 0;
//	int k = 0;
//	scanf("%d %d",&n,&k);
//	double ret = Pow(n,k);
//	printf("%lf\n", ret);
//
//	return 0;
//}
//---------------------------------------递归和非递归分别实现求n的阶乘（不考虑溢出的问题）
//long long Fac(int N)
//{
//    if (N <= 1)
//        return 1;
//
//    return Fac(N - 1) * N;
//}
//long long Fac(int N)
//{
//    long long ret = 1;
//    for (int i = 2; i <= N; ++i)
//    {
//        ret *= i;
//    }
//
//    return ret;
//}
//int main()
//{
//    int n = 0;
//    scanf("%d",&n);
//    int ret = Fac(n);
//    printf("%d",ret);
//    return  0;
//}
//-----------------------------斐波那契数列
//long long Fac(int N)
//{
//    if (N < 3)
//        return 1;
//
//    return Fac(N - 1) + Fac(N - 2);
//}
//int main()
//{
//    int n = 0;
//    scanf("%d",&n);
//   int ret = Fac(n);
//   printf("%d",ret);
//    return 0;
//}
void rv(int *arr,int *arr2)
{
	while (*arr!='\0') 
	{
		int temp = *arr;
		*arr = *arr2;
		*arr2 = temp;
		arr++;
		arr2++ ;
	}
	
}


int main()
{
		int arr1[10] = { 0 };
		int arr2[10] = { 0 };
		int i = 0;
		printf("请输入10个数字:>");
		for (i = 0; i < 10; i++)
		{
			scanf("%d", &arr1[i]);
		}
		printf("请输入10个数字:>");
		for (i = 0; i < 10; i++)
		{
			scanf("%d", &arr2[i]);
		}
		//交换
		for (i = 0; i < 10; i++)
		{
			int tmp = arr1[i];
			arr1[i] = arr2[i];
			arr2[i] = tmp;
		}
		return 0;
}
