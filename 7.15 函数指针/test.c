#define _CRT_SECURE_NO_WARNINGS 1



#include <stdio.h>
//void test()
//{
//	printf("hehe\n");
//}
//int main()
//{
//	printf("%p\n", test);
//	printf("%p\n", &test);
//	return 0;
//}

//int add(int x ,int y) 
//{
//	return x + y;
//}
//
//int main()
//{
//	int ret = add(3,5);
//	int (*pret) (int, int) = &add;
//	printf("%p",pret);
//	return 0;
//}

//void test(char* str)
//{
//
//}
//int main() 
//{
//	void(*pt)(char *) = &test;
//
//	return 0;
//}

int add(int x, int y)
{
	return x + y;
}

int main()
{
	int ret = add(3, 5);
	int (*pret) (int, int) = &add;  //存放函数的指针
	//返回类型 (*变量名) (函数类型)
	int prt1 = (*****pret)(3, 5);
	int prt2 = pret(3,5);
	int pt = add(3, 5); //ok  通过函数名
	printf("%d\n", prt1); // ok
	printf("%d\n", prt2); //ok
	printf("%d\n", pt); //ok

	return 0;
}